CC = cl
RM = del
TARGETS = main
OBJECTS = build\intermediate\*.obj
INCLUDES = include
COMPILER_FLAGS_D = /D_DEBUG /D_CRT_SECURE_NO_WARNINGS /wd4142 /I $(INCLUDES) /W3 /EHsc /c /Zi /Od /Tp
COMPILER_FLAGS = /D_CRT_SECURE_NO_WARNINGS /wd4142 /I $(INCLUDES) /W3 /EHsc /c /Zi /Ox /Tp
LINKER_FLAGS_D = /SUBSYSTEM:CONSOLE /LIBPATH:lib
LINKER_FLAGS = /SUBSYSTEM:WINDOWS /LIBPATH:lib
LIBS = SDL2main.lib SDL2.lib SDL2_ttf.lib SDL2_mixer.lib opengl32.lib glew32.lib glew32s.lib freetype.lib

EXE_NAME = tetris3D.exe

.default: all

.PHONY: run

all: $(TARGETS) link

link:
	LINK $(LINKER_FLAGS_D) $(OBJECTS) $(LIBS) /OUT:build\$(EXE_NAME)

main:
	$(CC) $(COMPILER_FLAGS) src\main.cpp /Fobuild\intermediate\main.obj 

run:
	cd build && $(EXE_NAME)
