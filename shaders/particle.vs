#version 330 core
layout (location = 0) in vec3 position;
out vec4 ParticleColor;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 color;
void main()
{
    float scale = 0.5;
    ParticleColor = vec4(color, 1.0f);
    gl_Position = projection * view * model * vec4((position * scale), 1.0);
}
