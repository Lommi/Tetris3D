#include <iostream>
#include <map>
#include <string>
#include <stdio.h>
#include <time.h>
#include "SDL2/SDL.h"
#include "GL/glew.h"
#include "GL/glu.h"
#include "SDL2/SDL_opengl.h"
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace glm;

#define SCREEN_W 416
#define SCREEN_H 640
#define CUBE_FLOATS 36 * 6
#define QUAD_FLOATS 3 * 6
#define NUM_SHAPES 300
#define NUM_BLOCKS 4 * NUM_SHAPES
#define NUM_PARTICLES 500
#define BLOCKS_IN_SHAPE 4
#define XLIMIT 11.0f
#define LEVELW 12
#define LEVELH 18
#define PARTICLE_SPEED 0.005f

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef struct Block Block;
typedef struct Shape Shape;
typedef struct Mesh Mesh;
typedef struct MeshBox MeshBox;
typedef struct Shader Shader;
typedef struct Character Character;
typedef struct Particle Particle;

enum Shapes
{
    SHAPE_I = 0,
    SHAPE_T,
    SHAPE_O,
    SHAPE_RL,
    SHAPE_LL,
    SHAPE_RZ,
    SHAPE_LZ,
    NUM_SHAPESTYPES
};

enum Angle
{
    A_TOP = 0,
    A_RIGHT,
    A_DOWN,
    A_LEFT
};

struct Mesh
{
    GLuint vao;
    GLuint vbo;
};

struct Shader
{
    GLuint program;
    GLuint vs;
    GLuint fs;
};

struct Block
{
    bool active;
    int type;
    vec3 pos = vec3(0.0f);
    mat4 model = mat4(1.0f);
};

struct Shape
{
    bool active;
    int type;
    int angle;
    vec3 lightColor = vec3(0.0f);
    vec3 diffuseColor = vec3(0.0f);
    vec3 ambientColor = vec3(0.0f);
    Block blocks[BLOCKS_IN_SHAPE];
};

struct Character
{
    GLuint TextureID;   // ID handle of the glyph texture
    glm::ivec2 Size;    // Size of glyph
    glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
    GLuint Advance;    // Horizontal offset to advance to next glyph
};

struct Particle
{
    bool active;
    GLfloat life = 0.0f;
    vec3 pos = vec3(0.0f);
    vec3 velocity = vec3(0.0f);
    vec3 color = vec3(0.0f);
    mat4 model = mat4(1.0f);
};

const char *boxShaderV = "#version 330 core\n"
"layout (location = 0) in vec3 position;\n"
"layout (location = 1) in vec3 normal;\n"
"out vec3 FragPos;\n"
"out vec3 Normal;\n"
"uniform mat4 model;\n"
"uniform mat4 view;\n"
"uniform mat4 projection;\n"
"void main()\n"
"{\n"
"    FragPos = vec3(model * vec4(position, 1.0));\n"
"    Normal = mat3(transpose(inverse(model))) * normal;\n"
"    gl_Position = projection * view * vec4(FragPos, 1.0f);\n"
"}\n";

const char *boxShaderF = "#version 330 core\n"
"out vec4 FragColor;\n"
"struct Material\n"
"{\n"
"    vec3 ambient;\n"
"    vec3 diffuse;\n"
"    vec3 specular;\n"
"    float shininess;\n"
"};\n"
"struct Light\n"
"{\n"
"    vec3 position;\n"
"    vec3 ambient;\n"
"    vec3 diffuse;\n"
"    vec3 specular;\n"
"};\n"
"in vec3 FragPos;\n"
"in vec3 Normal;\n"
"uniform vec3 viewPos;\n"
"uniform Material material;\n"
"uniform Light light;\n"

"void main()\n"
"{\n"
"    //ambient\n"
"    vec3 ambient = light.ambient * material.ambient;\n"
"    //diffuse\n"
"    vec3 norm = normalize(Normal);\n"
"    vec3 lightDir = normalize(light.position - FragPos);\n"
"    float diff = max(dot(norm, lightDir), 0.0);\n"
"    vec3 diffuse = light.diffuse * (diff * material.diffuse);\n"
"    //specular\n"
"    vec3 viewDir = normalize(viewPos - FragPos);\n"
"    vec3 reflectDir = reflect(-lightDir, norm);\n"
"    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);\n"
"    vec3 specular = light.specular * (spec * material.specular);\n"
"    vec3 result = ambient + diffuse + specular;\n" 
"    FragColor = vec4(result, 1.0f);\n"
"}\n";

const char *lampShaderV = "#version 330 core\n"
"layout (location = 0) in vec3 position;\n"
"uniform mat4 model;\n"
"uniform mat4 view;\n"
"uniform mat4 projection;\n"
"void main()\n"
"{\n"
"    gl_Position = projection * view * model * vec4(position, 1.0f);\n"
"}\n";

const char *lampShaderF = "#version 330 core\n"
"out vec4 FragColor;\n"
"void main()\n"
"{\n"
"    FragColor = vec4(1.0);\n"
"}\n";

const char *textShaderV = "#version 330 core\n"
"layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>\n"
"out vec2 TexCoords;\n"
"uniform mat4 projection;\n"
"void main()\n"
"{\n"
"    gl_Position = projection * vec4(vertex.xy, 0.0, 1.0);\n"
"    TexCoords = vertex.zw;\n"
"}\n";

const char *textShaderF = "#version 330 core\n"
"in vec2 TexCoords;\n"
"out vec4 color;\n"
"uniform sampler2D text;\n"
"uniform vec3 textColor;\n"
"void main()\n"
"{\n"
"    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);\n"
"    color = vec4(textColor, 1.0) * sampled;\n"
"}\n";

const char *particleShaderV = "#version 330 core\n"
"layout (location = 0) in vec3 position;\n"
"out vec4 ParticleColor;\n"
"uniform mat4 projection;\n"
"uniform mat4 view;\n"
"uniform mat4 model;\n"
"uniform vec3 color;\n"
"void main()\n"
"{\n"
"    float scale = 0.5;\n"
"    ParticleColor = vec4(color, 1.0f);\n"
"    gl_Position = projection * view * model * vec4((position * scale), 1.0);\n"
"}\n";

const char *particleShaderF = "#version 330 core\n"
"in vec4 ParticleColor;\n"
"out vec4 color;\n"
"void main()\n"
"{\n"
"    color = ParticleColor;\n"
"}\n";

const char *vsSource;
const char *fsSource;

bool gameloop;
static bool screenShakeActive;
static SDL_Window *window;
static SDL_GLContext glContext;
static FT_Library ft;
static FT_Face face;
static std::map<GLchar, Character> characters;
static std::string scoreStr;

static int posArray[LEVELH][LEVELW] = {0};
static int gameSpeed;
static int activeShapes = -1;
static int activeParticles;
static int score;
static int texWidth, texHeight, texChannels;
static float deltaTime, currentFrame, lastFrame = (float)SDL_GetTicks();
static float frameCount;
static float shakeVal;

static Shape *currentShape;
static Shape shapes[NUM_SHAPES];

static Particle particles[NUM_PARTICLES];

static mat4 model = mat4(1.0f);
static mat4 lamp = mat4(1.0f);
static mat4 view = mat4(1.0f);
static mat4 projection = mat4(1.0f);
static mat4 oProjection = mat4(1.0f);

static vec3 modelStartPos = vec3(5.0f, 17.0f, 0.0f);
static vec3 modelPos = modelStartPos;

static vec3 lightPos = vec3(6.0f, 8.0f, 50.0f);
static vec3 lightColor = vec3(0.0f);
static vec3 diffuseColor = vec3(0.0f);
static vec3 ambientColor = vec3(0.0f);

static vec3 cameraPos = vec3(-5.5f, -9.0f, -24.0f);
static vec3 cameraTarget = vec3(0.0f, 0.0f, 0.0f);
static vec3 cameraDir = normalize(cameraPos - cameraTarget);

static uint modelLoc;
static uint lampLoc;
static uint viewLoc;

static Mesh meshCube;
static Mesh meshLamp;
static Mesh meshText;
static Mesh meshParticle;
static Shader shaderCube;
static Shader shaderLamp;
static Shader shaderText;
static Shader shaderParticle;

static float cubeVertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    };

    float quadVertices[] = {
         0.5f,  0.5f, 0.0f,
         0.5f, -0.5f, 0.0f,
        -0.5f,  0.5f, 0.0f,
         0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,
        -0.5f,  0.5f, 0.0f
    };

int  init();
int  shutdown();
void input();
void update();
void render();

Mesh meshCreate(float *floatArray, uint numFloats, GLuint size,
    GLuint stride, GLuint pOffset, bool hack);
Shader shaderCreate(const char *vsText, const char *fsText);
void meshDestroy(Mesh *m);
void shaderDestroy(Shader *s);
void checkShaderError(GLuint shader, GLuint flag, bool isProgram,
    const char *errorMsg);
void RenderText(Shader &shader, std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

inline bool checkPos(int posX, int posY);
inline void displayClear(float r, float g, float b, float a);
inline void meshDraw(Mesh *m, uint numVertices);
inline void setCameraPos(float x, float y, float z);
inline void setCameraDir(float x, float y, float z);
inline void getDelta();
inline void newShape();
inline void checkLines();
inline float approach(float goal, float current, float s);

int main(int argc, char **argv)
{
    init();
    while(gameloop)
    {
        getDelta();
        input();
        update();
        render();
    }
    shutdown();
    return 0;
}

int init()
{
    srand((uint)time(0));
    gameloop = 1;

    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE);

    window = SDL_CreateWindow("Tetris3D", SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED, SCREEN_W, SCREEN_H, SDL_WINDOW_OPENGL);
    if (!window) puts("Window create failed");

    glContext = SDL_GL_CreateContext(window);
    if (!glContext) puts("SDL_GL context create failed");

    glewExperimental = GL_TRUE;
    GLenum status = glewInit();
    if (status != GLEW_OK)
        printf("GLEW failed to init\n");

    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, SCREEN_W, SCREEN_H);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

    shaderCube = shaderCreate(boxShaderV, boxShaderF);
    shaderLamp = shaderCreate(lampShaderV, lampShaderF);
    shaderText = shaderCreate(textShaderV, textShaderF);
    shaderParticle = shaderCreate(particleShaderV, particleShaderF);

    oProjection = ortho(0.0f, static_cast<GLfloat>(SCREEN_W),
        0.0f, static_cast<GLfloat>(SCREEN_H));
    glUseProgram(shaderText.program);
    glUniformMatrix4fv(glGetUniformLocation(shaderText.program, "projection"), 1, GL_FALSE, value_ptr(oProjection));

    if(FT_Init_FreeType(&ft)) {
      fprintf(stderr, "Could not init freetype library\n");
      return 1;
    }

    if(FT_New_Face(ft, "../assets/GermaniaOne-Regular.TTF", 0, &face)) {
      fprintf(stderr, "Could not open font\n");
      return 1;
    }

    FT_Set_Pixel_Sizes(face, 0, 32);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    /* FROM learnopengl.com */
    for (GLubyte c = 0; c < 128; c++)
    {
        // Load character glyph
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }
        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer
        );
        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // Now store character for later use
        Character character = {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            (GLuint)face->glyph->advance.x
        };
        characters.insert(std::pair<GLchar, Character>(c, character));
    }
/* ------------------------------ */

    FT_Done_Face(face);
    FT_Done_FreeType(ft);
/*
    if (SDL_GL_SetSwapInterval( 1 ) < 0 )
        printf( "Warning: Unable to set VSync! SDL Error: %s\n",
            SDL_GetError() );
*/
    glGenVertexArrays(1, &meshText.vao);
    glGenBuffers(1, &meshText.vbo);
    glBindVertexArray(meshText.vao);
    glBindBuffer(GL_ARRAY_BUFFER, meshText.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    scoreStr = "SCORE: 0";

    meshCube = meshCreate(cubeVertices, CUBE_FLOATS, 3, 6, 0, 1);
    meshLamp = meshCreate(cubeVertices, CUBE_FLOATS, 3, 6, 0, 0);
    meshParticle = meshCreate(quadVertices, QUAD_FLOATS, 3, 3, 0, 0);

    view = translate(mat4(1.0f), cameraPos);

    lamp = translate(lamp, lightPos);
    lamp = scale(lamp, vec3(0.5f));

    gameSpeed = 600;

    for (int i = 0; i < NUM_SHAPES; ++i)
    {
        for (int j = 0; j < BLOCKS_IN_SHAPE; ++j)
        {
            shapes[i].blocks[j].pos = vec3(0.0f, -100.0, 0.0f);
            shapes[i].blocks[j].model = translate(mat4(1.0f),
                shapes[i].blocks[j].pos);
        }
    }

    for (int i = 0; i < NUM_PARTICLES; ++i)
    {
        particles[i].pos = vec3(0.0f, 0.0f, 0.0);
        particles[i].model = translate(mat4(1.0f), particles[i].pos);
    }

    newShape();

    glUseProgram(shaderLamp.program);
    glUniformMatrix4fv(glGetUniformLocation(shaderLamp.program, "projection"),
        1, GL_FALSE, &projection[0][0]);
    viewLoc = glGetUniformLocation(shaderLamp.program, "view");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
    lampLoc = glGetUniformLocation(shaderLamp.program, "model");
    glUniformMatrix4fv(lampLoc, 1, GL_FALSE, value_ptr(lamp));

    return 0;
}

int shutdown()
{
    vsSource = 0;
    fsSource = 0;
    delete(vsSource);
    delete(fsSource);
    meshDestroy(&meshCube);
    meshDestroy(&meshLamp);
    shaderDestroy(&shaderCube);
    shaderDestroy(&shaderLamp);
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}

void input()
{
    SDL_Event e;
    while(SDL_PollEvent(&e))
    {
        switch(e.type)
        {
            case SDL_QUIT: gameloop = 0; break;
            case SDL_KEYDOWN:
            {
                switch(e.key.keysym.sym)
                {
                    case SDLK_ESCAPE: gameloop = 0; break;
                    case SDLK_RIGHT:
                    case SDLK_d:
                    {
                         if (currentShape->blocks[0].pos.x < XLIMIT &&
                             currentShape->blocks[1].pos.x < XLIMIT &&
                             currentShape->blocks[2].pos.x < XLIMIT &&
                             currentShape->blocks[3].pos.x < XLIMIT &&
                            !checkPos(
                                (int)(currentShape->blocks[0].pos.x + 1.0f),
                                (int)currentShape->blocks[0].pos.y) &&
                            !checkPos(
                                (int)(currentShape->blocks[1].pos.x + 1.0f),
                                (int)currentShape->blocks[1].pos.y) &&
                            !checkPos(
                                (int)(currentShape->blocks[2].pos.x + 1.0f),
                                (int)currentShape->blocks[2].pos.y) &&
                            !checkPos(
                                (int)(currentShape->blocks[3].pos.x + 1.0f),
                                (int)currentShape->blocks[3].pos.y))
                        {
                            for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
                            {
                                currentShape->blocks[i].pos.x += 1.0f;
                                currentShape->blocks[i].model = translate(
                                    mat4(1.0f), currentShape->blocks[i].pos);
                            }
                        }
                    }break;
                    case SDLK_LEFT:
                    case SDLK_a:
                    {
                         if (currentShape->blocks[0].pos.x > 0.0f &&
                             currentShape->blocks[1].pos.x > 0.0f &&
                             currentShape->blocks[2].pos.x > 0.0f &&
                             currentShape->blocks[3].pos.x > 0.0f &&
                            !checkPos(
                                (int)(currentShape->blocks[0].pos.x - 1.0f),
                                (int)currentShape->blocks[0].pos.y) &&
                            !checkPos(
                                (int)(currentShape->blocks[1].pos.x - 1.0f),
                                (int)currentShape->blocks[1].pos.y) &&
                            !checkPos(
                                (int)(currentShape->blocks[2].pos.x - 1.0f),
                                (int)currentShape->blocks[2].pos.y) &&
                            !checkPos(
                                (int)(currentShape->blocks[3].pos.x - 1.0f),
                                (int)currentShape->blocks[3].pos.y))
                        {
                            for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
                            {
                                currentShape->blocks[i].pos.x -= 1.0f;
                                currentShape->blocks[i].model = translate(
                                    mat4(1.0f), currentShape->blocks[i].pos);
                            }
                        }
                    }break;
                    case SDLK_DOWN:
                    case SDLK_s:
                    {
                        frameCount = (float)gameSpeed;
                    }break;
                    case SDLK_UP:
                    case SDLK_w: //0 = most left block 2 = most right
                    {
                        bool success = 0;
                        switch(currentShape->type)
                        {
                            case SHAPE_I:
                            {
                                switch(currentShape->angle)
                                {
                                    case A_TOP:
                                    case A_DOWN:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[1].pos.x - 1.0f),
                                            (int)(currentShape->blocks[1].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[2].pos.x - 2.0f),
                                            (int)(currentShape->blocks[2].pos.y - 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x - 3.0f),
                                            (int)(currentShape->blocks[3].pos.y - 3.0f)) &&
                                            (currentShape->blocks[3].pos.y - 3.0f >= 0.0f))
                                        {
                                            currentShape->blocks[1].pos.x -= 1.0f;
                                            currentShape->blocks[2].pos.x -= 2.0f;
                                            currentShape->blocks[3].pos.x -= 3.0f;
                                            currentShape->blocks[1].pos.y -= 1.0f;
                                            currentShape->blocks[2].pos.y -= 2.0f;
                                            currentShape->blocks[3].pos.y -= 3.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_RIGHT:
                                    case A_LEFT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[1].pos.x + 1.0f),
                                            (int)(currentShape->blocks[1].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[2].pos.x + 2.0f),
                                            (int)(currentShape->blocks[2].pos.y + 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x + 3.0f),
                                            (int)(currentShape->blocks[3].pos.y + 3.0f)) &&
                                            (currentShape->blocks[3].pos.x + 3.0f <= XLIMIT))
                                        {
                                            currentShape->blocks[1].pos.x += 1.0f;
                                            currentShape->blocks[2].pos.x += 2.0f;
                                            currentShape->blocks[3].pos.x += 3.0f;
                                            currentShape->blocks[1].pos.y += 1.0f;
                                            currentShape->blocks[2].pos.y += 2.0f;
                                            currentShape->blocks[3].pos.y += 3.0f;
                                            success = 1;
                                        }
                                    }break;
                                }
                            }break;
                            case SHAPE_T:
                            {
                                switch(currentShape->angle)
                                {
                                    case A_TOP:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x + 1.0f),
                                            (int)(currentShape->blocks[0].pos.y - 1.0f)) &&
                                            (currentShape->blocks[0].pos.y - 1.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x += 1.0f;
                                            currentShape->blocks[0].pos.y -= 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_RIGHT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x - 1.0f),
                                            (int)(currentShape->blocks[0].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x),
                                            (int)(currentShape->blocks[3].pos.y - 2.0f)) &&
                                        (currentShape->blocks[0].pos.x - 1.0f >= 0.0f) &&
                                        (currentShape->blocks[3].pos.y - 2.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x -= 1.0f;
                                            currentShape->blocks[0].pos.y += 1.0f;
                                            currentShape->blocks[3].pos.y -= 2.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_DOWN:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[2].pos.x - 1.0f),
                                            (int)(currentShape->blocks[2].pos.y + 1.0f)))
                                        {
                                            currentShape->blocks[2].pos.x -= 1.0f;
                                            currentShape->blocks[2].pos.y += 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_LEFT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[2].pos.x + 1.0f),
                                            (int)(currentShape->blocks[2].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x),
                                            (int)(currentShape->blocks[3].pos.y + 2.0f)) &&
                                        (currentShape->blocks[2].pos.x + 1.0f <= XLIMIT))
                                        {
                                            currentShape->blocks[2].pos.x += 1.0f;
                                            currentShape->blocks[2].pos.y -= 1.0f;
                                            currentShape->blocks[3].pos.y += 2.0f;
                                            success = 1;
                                        }
                                    }break;
                                }
                            }break;
                            case SHAPE_O: break;
                            case SHAPE_RL:
                            {
                                switch(currentShape->angle)
                                {
                                    case A_TOP:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x + 2.0f),
                                            (int)(currentShape->blocks[0].pos.y + 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x + 1.0f),
                                            (int)(currentShape->blocks[1].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x + 1.0f),
                                            (int)(currentShape->blocks[3].pos.y - 1.0f)) &&
                                        (currentShape->blocks[3].pos.x + 1.0f <= XLIMIT))
                                        {
                                            currentShape->blocks[0].pos.x += 2.0f;
                                            currentShape->blocks[0].pos.y += 2.0f;
                                            currentShape->blocks[1].pos.x += 1.0f;
                                            currentShape->blocks[1].pos.y += 1.0f;
                                            currentShape->blocks[3].pos.x += 1.0f;
                                            currentShape->blocks[3].pos.y -= 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_RIGHT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x + 2.0f),
                                            (int)(currentShape->blocks[0].pos.y - 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x + 1.0f),
                                            (int)(currentShape->blocks[1].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x - 1.0f),
                                            (int)(currentShape->blocks[3].pos.y - 1.0f)) &&
                                        (currentShape->blocks[0].pos.x + 2.0f <= XLIMIT) &&
                                        (currentShape->blocks[3].pos.y - 1.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x += 2.0f;
                                            currentShape->blocks[0].pos.y -= 2.0f;
                                            currentShape->blocks[1].pos.x += 1.0f;
                                            currentShape->blocks[1].pos.y -= 1.0f;
                                            currentShape->blocks[3].pos.x -= 1.0f;
                                            currentShape->blocks[3].pos.y -= 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_DOWN:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x - 2.0f),
                                            (int)(currentShape->blocks[0].pos.y - 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x - 1.0f),
                                            (int)(currentShape->blocks[1].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x - 1.0f),
                                            (int)(currentShape->blocks[3].pos.y + 1.0f)) &&
                                        (currentShape->blocks[0].pos.y - 2.0f >= 0.0f) &&
                                        (currentShape->blocks[3].pos.x - 1.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x -= 2.0f;
                                            currentShape->blocks[0].pos.y -= 2.0f;
                                            currentShape->blocks[1].pos.x -= 1.0f;
                                            currentShape->blocks[1].pos.y -= 1.0f;
                                            currentShape->blocks[3].pos.x -= 1.0f;
                                            currentShape->blocks[3].pos.y += 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_LEFT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x - 2.0f),
                                            (int)(currentShape->blocks[0].pos.y + 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x - 1.0f),
                                            (int)(currentShape->blocks[1].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x + 1.0f),
                                            (int)(currentShape->blocks[3].pos.y + 1.0f)) &&
                                        (currentShape->blocks[0].pos.x - 2.0f >= 0.0f) &&
                                        (currentShape->blocks[3].pos.y + 1.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x -= 2.0f;
                                            currentShape->blocks[0].pos.y += 2.0f;
                                            currentShape->blocks[1].pos.x -= 1.0f;
                                            currentShape->blocks[1].pos.y += 1.0f;
                                            currentShape->blocks[3].pos.x += 1.0f;
                                            currentShape->blocks[3].pos.y += 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                }
                            }break;
                            case SHAPE_LL:
                            {
                                switch(currentShape->angle)
                                {
                                    case A_TOP:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x + 2.0f),
                                            (int)(currentShape->blocks[0].pos.y + 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x + 1.0f),
                                            (int)(currentShape->blocks[1].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x - 1.0f),
                                            (int)(currentShape->blocks[3].pos.y + 1.0f)) &&
                                        (currentShape->blocks[3].pos.x - 1.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x += 2.0f;
                                            currentShape->blocks[0].pos.y += 2.0f;
                                            currentShape->blocks[1].pos.x += 1.0f;
                                            currentShape->blocks[1].pos.y += 1.0f;
                                            currentShape->blocks[3].pos.x -= 1.0f;
                                            currentShape->blocks[3].pos.y += 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_RIGHT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x + 2.0f),
                                            (int)(currentShape->blocks[0].pos.y - 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x + 1.0f),
                                            (int)(currentShape->blocks[1].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x - 1.0f),
                                            (int)(currentShape->blocks[3].pos.y - 1.0f)) &&
                                        (currentShape->blocks[0].pos.x + 2.0f <= XLIMIT))
                                        {
                                            currentShape->blocks[0].pos.x += 2.0f;
                                            currentShape->blocks[0].pos.y -= 2.0f;
                                            currentShape->blocks[1].pos.x += 1.0f;
                                            currentShape->blocks[1].pos.y -= 1.0f;
                                            currentShape->blocks[3].pos.x += 1.0f;
                                            currentShape->blocks[3].pos.y += 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_DOWN:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x - 2.0f),
                                            (int)(currentShape->blocks[0].pos.y - 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x - 1.0f),
                                            (int)(currentShape->blocks[1].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x - 1.0f),
                                            (int)(currentShape->blocks[3].pos.y + 1.0f)) &&
                                        (currentShape->blocks[0].pos.y - 2.0f >= 0.0f) &&
                                        (currentShape->blocks[3].pos.x + 1.0f <= XLIMIT))
                                        {
                                            currentShape->blocks[0].pos.x -= 2.0f;
                                            currentShape->blocks[0].pos.y -= 2.0f;
                                            currentShape->blocks[1].pos.x -= 1.0f;
                                            currentShape->blocks[1].pos.y -= 1.0f;
                                            currentShape->blocks[3].pos.x += 1.0f;
                                            currentShape->blocks[3].pos.y -= 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                    case A_LEFT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x - 2.0f),
                                            (int)(currentShape->blocks[0].pos.y + 2.0f)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x - 1.0f),
                                            (int)(currentShape->blocks[1].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x + 1.0f),
                                            (int)(currentShape->blocks[3].pos.y + 1.0f)) &&
                                        (currentShape->blocks[0].pos.x - 2.0f >= 0.0f) &&
                                        (currentShape->blocks[3].pos.y - 1.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x -= 2.0f;
                                            currentShape->blocks[0].pos.y += 2.0f;
                                            currentShape->blocks[1].pos.x -= 1.0f;
                                            currentShape->blocks[1].pos.y += 1.0f;
                                            currentShape->blocks[3].pos.x -= 1.0f;
                                            currentShape->blocks[3].pos.y -= 1.0f;
                                            success = 1;
                                        }
                                    }break;
                                }
                            }break;
                            case SHAPE_RZ:
                            {
                                switch(currentShape->angle)
                                {
                                    case A_TOP:
                                    case A_DOWN:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x + 1.0f),
                                            (int)(currentShape->blocks[0].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[2].pos.x + 1.0f),
                                            (int)(currentShape->blocks[2].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x),
                                            (int)(currentShape->blocks[3].pos.y - 2.0f)) &&
                                        (currentShape->blocks[3].pos.y - 2.0f >= 0.0f))
                                        {
                                             currentShape->blocks[0].pos.x += 1.0f;
                                             currentShape->blocks[0].pos.y += 1.0f;
                                             currentShape->blocks[2].pos.x += 1.0f;
                                             currentShape->blocks[2].pos.y -= 1.0f;
                                             currentShape->blocks[3].pos.y -= 2.0f;
                                             success = 1;
                                        }
                                    }break;
                                    case A_RIGHT:
                                    case A_LEFT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x - 1.0f),
                                            (int)(currentShape->blocks[0].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[2].pos.x - 1.0f),
                                            (int)(currentShape->blocks[2].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x),
                                            (int)(currentShape->blocks[3].pos.y + 2.0f)) &&
                                        (currentShape->blocks[0].pos.x - 1.0f >= 0.0f))
                                        {
                                            currentShape->blocks[0].pos.x -= 1.0f;
                                            currentShape->blocks[0].pos.y -= 1.0f;
                                            currentShape->blocks[2].pos.x -= 1.0f;
                                            currentShape->blocks[2].pos.y += 1.0f;
                                            currentShape->blocks[3].pos.y += 2.0f;
                                            success = 1;
                                        }
                                    }break;
                                }
                            }break;
                            case SHAPE_LZ:
                            {
                                switch(currentShape->angle)
                                {
                                    case A_TOP:
                                    case A_DOWN:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x + 1.0f),
                                            (int)(currentShape->blocks[0].pos.y)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x),
                                            (int)(currentShape->blocks[1].pos.y - 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[2].pos.x - 1.0f),
                                            (int)(currentShape->blocks[2].pos.y)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x - 2.0f),
                                            (int)(currentShape->blocks[3].pos.y - 1.0f)) &&
                                        (currentShape->blocks[3].pos.y - 1.0f >= 0.0f))
                                        {
                                             currentShape->blocks[0].pos.x += 1.0f;
                                             currentShape->blocks[1].pos.y -= 1.0f;
                                             currentShape->blocks[2].pos.x -= 1.0f;
                                             currentShape->blocks[3].pos.x -= 2.0f;
                                             currentShape->blocks[3].pos.y -= 1.0f;
                                             success = 1;
                                        }
                                    }break;
                                    case A_RIGHT:
                                    case A_LEFT:
                                    {
                                        if(!checkPos((int)(currentShape->blocks[0].pos.x - 1.0f),
                                            (int)(currentShape->blocks[0].pos.y)) &&
                                        !checkPos((int)(currentShape->blocks[1].pos.x),
                                            (int)(currentShape->blocks[1].pos.y + 1.0f)) &&
                                        !checkPos((int)(currentShape->blocks[2].pos.x + 1.0f),
                                            (int)(currentShape->blocks[2].pos.y)) &&
                                        !checkPos((int)(currentShape->blocks[3].pos.x + 2.0f),
                                            (int)(currentShape->blocks[3].pos.y + 1.0f)) &&
                                        (currentShape->blocks[3].pos.x + 2.0f <= XLIMIT))
                                        {
                                             currentShape->blocks[0].pos.x -= 1.0f;
                                             currentShape->blocks[1].pos.y += 1.0f;
                                             currentShape->blocks[2].pos.x += 1.0f;
                                             currentShape->blocks[3].pos.x += 2.0f;
                                             currentShape->blocks[3].pos.y += 1.0f;
                                             success = 1;
                                        }
                                    }break;
                                }
                            }break;
                        }
                        if (success)
                        {
                            ++currentShape->angle;
                            if (currentShape->angle > A_LEFT)
                                currentShape->angle = A_TOP;
                            for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
                                currentShape->blocks[i].model = translate(mat4(1.0f),
                                    currentShape->blocks[i].pos);
                        }
                    }break;
                    case SDLK_RETURN:
                    case SDLK_SPACE:
                    {
                        float posY = 0.0f;
                        float dist = 0.0f;
                        float maxDist = 20.0f;
                        Shape *cs = currentShape;
                        for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
                        {
                            dist = 0.0f;
                            posY = cs->blocks[i].pos.y;
                            while (posY > 0.0f && posArray[(int)(posY - 1.0f)]
                                [(int)cs->blocks[i].pos.x] == 0)
                            {
                                posY -= 1.0f;
                                dist += 1.0f;
                            }
                            if (dist < maxDist)
                                maxDist = dist;
                        }
                        for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
                        {
                            cs->blocks[i].pos.y -= maxDist;
                            posArray[(int)cs->blocks[i].pos.y]
                                [(int)cs->blocks[i].pos.x] = 1;
                            cs->blocks[i].model = translate(mat4(1.0f),
                                cs->blocks[i].pos);
                        }
                        newShape();
                        cs = 0;
                        delete(cs);
                    }break;
                    case SDLK_m:
                    {
                        for (int a = 0; a < 4; ++a)
                        {
                            ++activeParticles;
                            if (activeParticles >= NUM_PARTICLES)
                                activeParticles = 0;
                            particles[activeParticles].active = 1;
                            particles[activeParticles].pos = currentShape->blocks[a].pos;
                            particles[activeParticles].life = 1.0f;
                            particles[activeParticles].model = translate(mat4(1.0f), particles[activeParticles].pos);
                            particles[activeParticles].velocity.x = (float)((-10 + rand() % 21) * PARTICLE_SPEED);
                            particles[activeParticles].velocity.y = (float)((-10 + rand() % 21) * PARTICLE_SPEED);
                            particles[activeParticles].velocity.z = (float)((-20 + rand() % 41) * PARTICLE_SPEED);
                            particles[activeParticles].color = currentShape->lightColor * 0.75f;
                        }
                    }break;
                    case SDLK_n:
                    {
                        screenShakeActive = 1;
                        shakeVal += 2.0f;
                    }break;
                }
            }break;
        }
    }
}

void update()
{
    Shape *cs = currentShape;
    frameCount += 1 * deltaTime;

    for (int i = 0; i < NUM_PARTICLES; ++i)
    {
        if (!particles[i].active) continue;
        particles[i].pos += particles[i].velocity * deltaTime;
        particles[i].model = translate(mat4(1.0f), particles[i].pos);
        particles[i].life -= 0.0025f * deltaTime;
        if (particles[i].life <= 0.0f)
            particles[i].active = 0;
    }

    if (screenShakeActive)
    {
        cameraPos = vec3(-5.5f, -9.0f, -24.0f);
        cameraPos.x += (-shakeVal + (rand() % (int)shakeVal * 2)) * 0.01f * deltaTime;
        cameraPos.y += (-shakeVal + (rand() % (int)shakeVal * 2)) * 0.01f * deltaTime;
        view = translate(mat4(1.0f), cameraPos);
        shakeVal = approach(0.0f, shakeVal, 0.01f * deltaTime);
        if (shakeVal <= 1.0f)
        {
            cameraPos = vec3(-5.5f, -9.0f, -24.0f);
            view = translate(mat4(1.0f), cameraPos);
            screenShakeActive = 0;
        }
    }

    if (frameCount > gameSpeed)
    {
        if ((cs->blocks[0].pos.y - 1.0f < 0.0f) ||
           (cs->blocks[1].pos.y - 1.0f < 0.0f) ||
           (cs->blocks[2].pos.y - 1.0f < 0.0f) ||
           (cs->blocks[3].pos.y - 1.0f < 0.0f))
        {
            for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
                posArray[(int)cs->blocks[i].pos.y][(int)cs->blocks[i].pos.x] = 1;
            newShape();
        }
        else if (checkPos((int)cs->blocks[0].pos.x,
                (int)(cs->blocks[0].pos.y - 1.0f)) ||
            checkPos((int)cs->blocks[1].pos.x,
                (int)(cs->blocks[1].pos.y - 1.0f)) ||
            checkPos((int)cs->blocks[2].pos.x,
                (int)(cs->blocks[2].pos.y - 1.0f)) ||
            checkPos((int)cs->blocks[3].pos.x,
                (int)(cs->blocks[3].pos.y - 1.0f)))
        {
            for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
                posArray[(int)cs->blocks[i].pos.y][(int)cs->blocks[i].pos.x] = 1;
            newShape();
        }
        else
        {
            for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
            {
                cs->blocks[i].pos.y -= 1.0f;
                currentShape->blocks[i].model = translate(mat4(1.0f),
                    currentShape->blocks[i].pos);
            }
        }
        frameCount = 0;
    }
    cs = 0;
    delete(cs);
}

void render()
{
    displayClear(0.0f, 0.0f, 0.0f, 1.0f);

    glUseProgram(shaderCube.program);
    glUniformMatrix4fv(glGetUniformLocation(shaderCube.program, "projection"),
        1, GL_FALSE, &projection[0][0]);

    glUniform3f(glGetUniformLocation(shaderCube.program, "light.position"),
        lightPos[0], lightPos[1], lightPos[2]);
    glUniform3f(glGetUniformLocation(shaderCube.program, "viewPos"),
        lightPos[0], lightPos[1], lightPos[2]);

    //Material properties
    glUniform3f(glGetUniformLocation(shaderCube.program, "material.ambient"),
        0.5f, 0.5f, 0.5f);
    glUniform3f(glGetUniformLocation(shaderCube.program, "material.diffuse"),
        0.5f, 0.5f, 0.5f);
    glUniform3f(glGetUniformLocation(shaderCube.program, "material.specular"),
        0.5f, 0.5f, 0.5f);
    glUniform1f(glGetUniformLocation(shaderCube.program, "material.shininess"),
        16.0f);

    projection = perspective(radians(45.0f), (float)SCREEN_W / (float)SCREEN_H,
        0.1f, 100.0f);
    viewLoc = glGetUniformLocation(shaderCube.program, "view");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);

    modelLoc = glGetUniformLocation(shaderCube.program, "model");

    for (int i = 0; i < NUM_SHAPES; ++i)
    {
        glUniform3f(glGetUniformLocation(shaderCube.program, "light.ambient"),
            shapes[i].ambientColor.x,
            shapes[i].ambientColor.y,
            shapes[i].ambientColor.z);
        glUniform3f(glGetUniformLocation(shaderCube.program, "light.diffuse"),
            shapes[i].diffuseColor.x,
            shapes[i].diffuseColor.y,
            shapes[i].diffuseColor.z);
        glUniform3f(glGetUniformLocation(shaderCube.program, "light.specular"),
            0.5f, 0.5f, 0.5f);
        for (int j = 0; j < BLOCKS_IN_SHAPE; ++j)
        {
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE,
                value_ptr(shapes[i].blocks[j].model));
            meshDraw(&meshCube, 36);
        }
    }
    for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
    {
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE,
            value_ptr(currentShape->blocks[i].model));
        meshDraw(&meshCube, 36);
    }

    glUseProgram(shaderParticle.program);
    glUniformMatrix4fv(glGetUniformLocation(shaderParticle.program,
        "projection"), 1, GL_FALSE, &projection[0][0]);
    viewLoc = glGetUniformLocation(shaderParticle.program, "view");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
    modelLoc = glGetUniformLocation(shaderParticle.program, "model");


    for (int i = 0; i < NUM_PARTICLES; ++i)
    {
        if (!particles[i].active) continue;
        glUniform3f(glGetUniformLocation(shaderParticle.program, "color"),
            particles[i].color.x, particles[i].color.y, particles[i].color.z);
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, value_ptr(particles[i].model));
        meshDraw(&meshParticle, 6);
    }

    RenderText(shaderText, scoreStr,
        0.0f, 616.0f, 1.0f, vec3(1.0, 1.0f, 1.0f));

    SDL_GL_SwapWindow(window);
}

Mesh meshCreate(float *floatArray, uint numFloats, GLuint size,
    GLuint stride, GLuint pOffset, bool hack)
{
    Mesh m;

    glGenVertexArrays(1, &m.vao);
    glGenBuffers(1, &m.vbo);
    glBindVertexArray(m.vao);
    glBindBuffer(GL_ARRAY_BUFFER, m.vbo);

    glBufferData(GL_ARRAY_BUFFER, sizeof(floatArray) * numFloats,
        floatArray, GL_STATIC_DRAW);
    glVertexAttribPointer(0, size, GL_FLOAT, GL_FALSE, stride * sizeof(float),
        (void*)(pOffset * sizeof(float)));
    glEnableVertexAttribArray(0);

    if (hack)
    {
        // normal attribute
        glVertexAttribPointer(1, size, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void*)(size * sizeof(float)));
        glEnableVertexAttribArray(1);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    return m;
}

Shader shaderCreate(const char *vsText, const char *fsText)
{
    if (!vsText || !fsText)
        printf("vsText or fsText is NULL\n");

    Shader s;

    vsSource = vsText;
    fsSource = fsText;

    s.vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(s.vs, 1, &vsSource, 0);
    glCompileShader(s.vs);
    checkShaderError(s.vs, GL_COMPILE_STATUS, 0,
        "Error: vShader compile failed");

    s.fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(s.fs, 1, &fsSource, 0);
    glCompileShader(s.fs);
    checkShaderError(s.fs, GL_COMPILE_STATUS, 0,
        "Error: fShader compile failed");

    s.program = glCreateProgram();
    glAttachShader(s.program, s.vs);
    glAttachShader(s.program, s.fs);
    glLinkProgram(s.program);
    checkShaderError(s.program, GL_LINK_STATUS, 1,
        "Error: Program linking failed");
    vsSource = 0;
    fsSource = 0;
    delete(vsSource);
    delete(fsSource);
    glDeleteShader(s.vs);
    glDeleteShader(s.fs);

    return s;
}

void meshDestroy(Mesh *m)
{
    glDeleteVertexArrays(1, &m->vao);
    glDeleteBuffers(1, &m->vbo);
}

void shaderDestroy(Shader *s)
{
    glDeleteProgram(s->program);
}

void checkShaderError(GLuint shader, GLuint flag, bool isProgram,
    const char *errorMsg)
{
    GLint success = 0;
    GLchar error[1024] = { 0 };

    if (isProgram)
        glGetProgramiv(shader, flag, &success);
    else
        glGetShaderiv(shader, flag, &success);

    if (success == GL_FALSE)
    {
        if (isProgram)
            glGetProgramInfoLog(shader, sizeof(error), 0, error);
        else
            glGetShaderInfoLog(shader, sizeof(error), 0, error);

        printf("%s : %s\n", errorMsg, error);
    }
}

inline bool checkPos(int posX, int posY)
{
    if (posArray[posY][posX])
        return true;
    else
        return false;
}

inline void displayClear(float r, float g, float b, float a)
{
    glClearColor(r, g, b, a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

inline void meshDraw(Mesh *m, uint numVertices)
{
    glBindVertexArray(m->vao);
    glDrawArrays(GL_TRIANGLES, 0, numVertices);
}

inline void setCameraPos(float x, float y, float z)
{
    cameraPos = vec3(x, y, z);
}

inline void setCameraDir(float x, float y, float z)
{
    cameraTarget = vec3(x, y, z);
    cameraDir = normalize(cameraPos - cameraTarget);
}

inline void getDelta()
{
    currentFrame = (float)SDL_GetTicks();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
}

inline void newShape()
{
    if (activeShapes > -1)
    {
        for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
        {
            shapes[activeShapes].blocks[i].pos = currentShape->blocks[i].pos;
        }
    }
    ++activeShapes;
    if (activeShapes > NUM_SHAPES - 1)
        activeShapes = 0;
    checkLines();
    currentShape = &shapes[activeShapes];
    shapes[activeShapes].active = 1;
    int type = rand() % NUM_SHAPESTYPES;
    currentShape->type = type;
    currentShape->angle = A_TOP;

    for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
    {
        currentShape->blocks[i].pos = modelStartPos;
    }

    switch(currentShape->type)
    {
        case SHAPE_I:
        {
            currentShape->blocks[1].pos.x += 1.0f;
            currentShape->blocks[2].pos.x += 2.0f;
            currentShape->blocks[3].pos.x += 3.0f;

            shapes[activeShapes].lightColor.x = 0.0f;
            shapes[activeShapes].lightColor.y = 1.0f;
            shapes[activeShapes].lightColor.z = 1.0f;
        }break;
        case SHAPE_T:
        {
            currentShape->blocks[1].pos.x += 1.0f;
            currentShape->blocks[2].pos.x += 2.0f;
            currentShape->blocks[3].pos.x += 1.0f;
            currentShape->blocks[3].pos.y += 1.0f;

            shapes[activeShapes].lightColor.x = 0.0f;
            shapes[activeShapes].lightColor.y = 1.0f;
            shapes[activeShapes].lightColor.z = 0.0f;
        }break;
        case SHAPE_O:
        {
            currentShape->blocks[1].pos.x += 1.0f;
            currentShape->blocks[2].pos.x += 1.0f;
            currentShape->blocks[2].pos.y += 1.0f;
            currentShape->blocks[3].pos.y += 1.0f;

            shapes[activeShapes].lightColor.x = 1.0f;
            shapes[activeShapes].lightColor.y = 1.0f;
            shapes[activeShapes].lightColor.z = 0.0f;
        }break;
        case SHAPE_RL:
        {
            currentShape->blocks[1].pos.x += 1.0f;
            currentShape->blocks[2].pos.x += 2.0f;
            currentShape->blocks[3].pos.x += 2.0f;
            currentShape->blocks[3].pos.y += 1.0f;

            shapes[activeShapes].lightColor.x = 1.0f;
            shapes[activeShapes].lightColor.y = 1.0f;
            shapes[activeShapes].lightColor.z = 1.0f;
        }break;
        case SHAPE_LL:
        {
            currentShape->blocks[1].pos.x += 1.0f;
            currentShape->blocks[2].pos.x += 2.0f;
            currentShape->blocks[3].pos.x += 2.0f;
            currentShape->blocks[3].pos.y -= 1.0f;

            shapes[activeShapes].lightColor.x = 0.0f;
            shapes[activeShapes].lightColor.y = 0.0f;
            shapes[activeShapes].lightColor.z = 1.0f;
        }break;
        case SHAPE_RZ:
        {
            currentShape->blocks[1].pos.x += 1.0f;
            currentShape->blocks[2].pos.x += 1.0f;
            currentShape->blocks[2].pos.y += 1.0f;
            currentShape->blocks[3].pos.x += 2.0f;
            currentShape->blocks[3].pos.y += 1.0f;

            shapes[activeShapes].lightColor.x = 1.0f;
            shapes[activeShapes].lightColor.y = 0.0f;
            shapes[activeShapes].lightColor.z = 1.0f;
        }break;
        case SHAPE_LZ:
        {
            currentShape->blocks[0].pos.y += 1.0f;
            currentShape->blocks[1].pos.x += 1.0f;
            currentShape->blocks[1].pos.y += 1.0f;
            currentShape->blocks[2].pos.x += 1.0f;
            currentShape->blocks[3].pos.x += 2.0f;

            shapes[activeShapes].lightColor.x = 1.0f;
            shapes[activeShapes].lightColor.y = 0.0f;
            shapes[activeShapes].lightColor.z = 0.0f;
        }break;
    }

    shapes[activeShapes].diffuseColor = shapes[activeShapes].lightColor *
        vec3(0.8f);
    shapes[activeShapes].ambientColor = shapes[activeShapes].diffuseColor *
        vec3(1.0f);

    for (int i = 0; i < BLOCKS_IN_SHAPE; ++i)
        currentShape->blocks[i].model = translate(mat4(1.0f),
            currentShape->blocks[i].pos);
}

inline void checkLines()
{
    bool cl = 0;
    int cr = 0;
    int tetrislen = 0;

    for (int j = 0; j < LEVELW - 1; ++j)
    {
        if (posArray[LEVELH][j] == 1)
            gameloop = 0;
    }
    for (int i = 0; i < LEVELH; ++i)
    {
        for (int j = 0; j < LEVELW; ++j)
        {
            if (posArray[i][j] == 0)
                break;
            if (j == LEVELW - 1)
            {
                ++tetrislen;
                cl = 1;
                if (i > cr)
                cr = i;
                for (int k = 0; k < NUM_SHAPES; ++k)
                {
                    for (int l = 0; l < BLOCKS_IN_SHAPE; ++l)
                    {
                        shapes[k].blocks[l].active = 1;
                        if ((int)shapes[k].blocks[l].pos.y == i)
                        {
                            for (int a = 0; a < 4; ++a)
                            {
                                ++activeParticles;
                                if (activeParticles >= NUM_PARTICLES)
                                    activeParticles = 0;
                                particles[activeParticles].active = 1;
                                particles[activeParticles].pos = shapes[k].blocks[l].pos;
                                particles[activeParticles].life = 1.0f;
                                particles[activeParticles].model = translate(mat4(1.0f), particles[activeParticles].pos);
                                particles[activeParticles].velocity.x = (float)((-10 + rand() % 21) * PARTICLE_SPEED);
                                particles[activeParticles].velocity.y = (float)((-10 + rand() % 21) * PARTICLE_SPEED);
                                particles[activeParticles].velocity.z = (float)((-20 + rand() % 41) * PARTICLE_SPEED);
                                particles[activeParticles].color = shapes[k].lightColor * 0.75f;
                            }
                            posArray[(int)shapes[k].blocks[l].pos.y]
                                [(int)shapes[k].blocks[l].pos.x] = 0;
                            shapes[k].blocks[l].pos.y = -100.0f;
                            shapes[k].blocks[l].model = translate(mat4(1.0f),
                                shapes[k].blocks[l].pos);
                            shapes[k].blocks[l].active = 0;
                        }
                    }
                }
            }
        }
    }
    if (!cl) return;
    if (gameSpeed > 60) gameSpeed -= 20;
    screenShakeActive = 1;
    shakeVal = 2.0f * tetrislen;
    if (tetrislen == 4) shakeVal = 10.0f;
    score += 50 * tetrislen * tetrislen;
    scoreStr = "SCORE: " + std::to_string(score);
    for (int i = cr; i < LEVELH; ++i)
    {
        for (int j = 0; j < LEVELW; ++j)
        {
            for (int k = 0; k < NUM_SHAPES; ++k)
            {
                for (int l = 0; l < BLOCKS_IN_SHAPE; ++l)
                {
                    if ((int)shapes[k].blocks[l].pos.y > 0 &&
                        (int)shapes[k].blocks[l].pos.y < LEVELH &&
                        (int)shapes[k].blocks[l].pos.y == i &&
                        shapes[k].blocks[l].active)
                    {
                        posArray[(int)shapes[k].blocks[l].pos.y]
                            [(int)shapes[k].blocks[l].pos.x] = 0;
                        shapes[k].blocks[l].pos.y -= 1.0f * tetrislen;
                        posArray[(int)shapes[k].blocks[l].pos.y]
                            [(int)shapes[k].blocks[l].pos.x] = 1;
                        shapes[k].blocks[l].model = translate(mat4(1.0f),
                            shapes[k].blocks[l].pos);
                        shapes[k].blocks[l].active = 0;
                    }
                }
            }
        }
    }
}

inline float approach(float goal, float current, float s)
{
    float difference = goal - current;

    if (difference > s)
        return current + s;
    if (difference < -s)
        return current - s;

    return goal;
}

void RenderText(Shader &shader, std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color)
{
    // Activate corresponding render state
    glUseProgram(shader.program);
    glUniform3f(glGetUniformLocation(shader.program, "textColor"), color.x, color.y, color.z);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(meshText.vao);

    // Iterate through all characters
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = characters[*c];

        GLfloat xpos = x + ch.Bearing.x * scale;
        GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

        GLfloat w = ch.Size.x * scale;
        GLfloat h = ch.Size.y * scale;
        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }
        };
        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, meshText.vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
